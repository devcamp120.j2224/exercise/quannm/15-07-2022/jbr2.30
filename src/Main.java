public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        Customer customer1 = new Customer(1, "Quan", 10);
        Customer customer2 = new Customer(2, "Boi", 15);
        System.out.println(customer1 + "," + customer2);

        Invoice invoice1 = new Invoice(1108, customer1, 500);
        Invoice invoice2 = new Invoice(0306, customer2, 800);
        System.out.println(invoice1 + "," + invoice2);

        invoice1.getAmoutAfterDiscount();
        invoice2.getAmoutAfterDiscount();
        System.out.println("After discount=" + invoice1 + "," + invoice2);
    }
}